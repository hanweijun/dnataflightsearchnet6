﻿using DnataLib.Entity;

namespace DnataLib.Repositories
{
    public interface IFlightRepository
    {
        IQueryable<FlightSegment> GetAllFlights();
        Task<IList<FlightSegment>> GetAllFlightsAsync();
        Task<FlightSegment[]> GetFlightsByFlightNosAsync(string[] flightNos);

        Task InsertIntoFlightSegments(FlightSegment[] flightSegments);
    }
}
