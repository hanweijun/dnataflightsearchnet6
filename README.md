# README #

This demo project is a WebAPI applicaiton in the latest .Net 6 using Visual Studio 2022. Actually, WebAPI application is better for this demo than console application. The source code of DnataLib and DnataLib.Test projects are almost the same as the last console application project in .Net 5. For migration, all the NuGet packages are updated to the latest versions for .Net 6. Anyway, this project also shows migrating an application from .Net 5 to .Net 6.

Compared to the last console application demo, some different features are below.

* 1. SeriLog saves logs to rolling files rather than console only. This is useful in production evironment for issue investigation.

* 2. Swagger for WebAPI. We can test the web api directly using Swagger web page.


## Please note running this project requires Visual Studio 2022 ##