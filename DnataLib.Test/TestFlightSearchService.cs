using DnataLib.Entity;
using DnataLib.Helpers;
using DnataLib.Repositories;
using DnataLib.Services;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DnataLib.Test
{
    // This test class shows using Moq to mock interfaces in unit test
    // Note: for demo purpose, only one test method is implemented in this class. We may add more when necessary
    [TestClass]
    public class TestFlightSearchService
    {
        [TestMethod]
        public async Task TestGetMoreThan2HourGroud()
        {
            var mockILogger = new Mock<ILogger<FlightSearchService>>();
            var mockIMemoryCache = new Mock<IMemoryCache>();

            var mockIFlightRepository = new Mock<IFlightRepository>();
            var t = Task.Run(() => BuildTestData()); 
            mockIFlightRepository.Setup(p => p.GetAllFlightsAsync()).Returns(t);

            var inMemorySettings = new Dictionary<string, string> {
                {"CacheMinutes", "5"},
            };
            IConfiguration configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(inMemorySettings)
                .Build();

            var service = new FlightSearchService(mockILogger.Object, configuration, mockIMemoryCache.Object, mockIFlightRepository.Object);

            var flights = await service.GetMoreThan2HourGroud();
            Assert.IsTrue(flights.Count == 2);
        }

        private static IList<FlightSegment> BuildTestData()
        {
            var flights = FlightBuilder.GetFlights();
            return ConvertHelper.ConvertToFlightSegmentList(flights);
        }
    }
}
