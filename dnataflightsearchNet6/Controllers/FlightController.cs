using DnataLib.Model;
using DnataLib.Services;
using Microsoft.AspNetCore.Mvc;

namespace dnataflightsearchNet6.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class FlightController : ControllerBase
    {

        private readonly ILogger<FlightController> _logger;
        private readonly IFlightSearchService _flightSearchService;

        public FlightController(ILogger<FlightController> logger, IFlightSearchService flightSearchService)
        {
            _logger = logger;
            _flightSearchService= flightSearchService;
        }

        [HttpGet(Name = "GetFlights")]
        public async Task<IEnumerable<Flight>?> Get(int rule)
        {
            _logger.LogInformation($"Calling service GetFlights...");

            try
            {
                switch(rule)
                {
                    case 1:
                        return await _flightSearchService.GetDepartbeforeCurrentDate();
                    case 2:
                        return await _flightSearchService.GetArrivalBeforeDeparture();
                    case 3:
                        return await _flightSearchService.GetMoreThan2HourGroud();
                    case 4:
                        return await _flightSearchService.GetByRuleOneAndTwo();
                    default:
                        Response.StatusCode = 400; // bad request
                        return null;
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex,"Error occurred.");
                Response.StatusCode = 500; // internal server error
            }
            return null;
        }
    }
}